import sys
import urllib
import requests

SAMPLE_URL = 'https://www.db.yugioh-card.com/yugiohdb/card_search.action?ope=1&sess=1&pid=1101000&rp=99999&request_locale=ja'

host = 'localhost:' + sys.argv[1]
url = 'http://' + host + '?q=' + urllib.parse.quote(SAMPLE_URL)
jsed_html = requests.get(url).text
with open('jsed.html', 'w') as f:
    f.write(jsed_html)

html = requests.get(SAMPLE_URL).text
with open('normal.html', 'w') as f:
    f.write(html)
