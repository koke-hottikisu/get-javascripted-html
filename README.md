# 概要
javascriptを実行したhtmlを持ってきてくれる、webサーバです。

リクエストボディは未対応です。

# クイックスタート
```
docker run -d -p 30080:80 --name get-javascripted-html --rm docker build registry.gitlab.com/koke-hottikisu/get-javascripted-html
python sample.py 30080
docker stop get-javascripted-html
```

# usage
http://hostname?q={url}
* url : パーセントエンコーディングされたurl
