FROM python:3.8-alpine

# ChromeDriver
RUN apk add chromium chromium-chromedriver

# flask
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY ./app /app
ENV FLASK_APP='/app/run.py'
ENTRYPOINT ["python", "-u", "-m", "flask", "run", "--host", "0.0.0.0", "--port", "80"]
