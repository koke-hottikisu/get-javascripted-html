import time
import urllib

from flask import Flask, request
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
# from werkzeug.middleware.profiler import ProfilerMiddleware

app = Flask(__name__)
'''
app.config['PROFILE'] = True
app.wsgi_app = ProfilerMiddleware(app.wsgi_app,
                                  sort_by=['tottime'],
                                  restrictions=[20])
'''
@app.route('/', methods=['GET'])
def search():
    if 'q' not in request.args:
        return '', 400
    q = request.args.get('q', type=str)
    q = urllib.parse.unquote(q)
    return jsed_html(q), 200


def jsed_html(url):
    """
    urlからjavascriptを実行したhtmlを返す
    """
    print(url)
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    driver = webdriver.Chrome(options=options)
    driver.get(url)
    time.sleep(0.8)
    html = driver.page_source
    driver.close()
    driver.quit()
    return html
